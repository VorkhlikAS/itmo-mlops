"""
Preprocess stars dataset.
"""

import argparse
import pandas as pd


def preprocess_df(star_df):
    """
    Preprocess the stars dataset in-memory.

    Parameters:
    - data_frame: Input pandas DataFrame.

    Returns:
    - Processed pandas DataFrame.
    """
    target = star_df["Star category"]
    features = star_df.drop(columns=["Star category"])

    features_df = pd.get_dummies(features)
    star_df = pd.concat([features_df, target], axis=1)
    print(star_df.head())

    return star_df


def preprocess(input_csv, output_csv):
    """
    Preprocess the stars dataset from file.

    Parameters:
    - input_csv: Path to the input CSV file.
    - output_csv: Path to the output preprocessed CSV file.
    """
    data_frame = pd.read_csv(input_csv)
    data_frame_processed = preprocess_df(data_frame)
    data_frame_processed.to_csv(output_csv, index=False)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Preprocess stars dataset")
    parser.add_argument(
        "input_csv", type=str, help="Path to input stars CSV file"
    )
    parser.add_argument(
        "output_csv", type=str, help="Path to output preprocessed CSV file"
    )
    args = parser.parse_args()
    preprocess(args.input_csv, args.output_csv)
