"""
Train a model on the stars dataset.
"""

import argparse
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
import joblib


def train_model(data_frame):
    """
    Train a RandomForest model on the stars dataset in-memory.

    Parameters:
    - data_frame: Input pandas DataFrame.

    Returns:
    - Trained RandomForest model.
    """
    features = data_frame.drop(columns=["Star category"])
    target = data_frame["Star category"]

    (
        features_train,
        _,
        target_train,
        _,
    ) = train_test_split(features, target, test_size=0.2, random_state=42)
    model = RandomForestClassifier(n_estimators=100, random_state=42)
    model.fit(features_train, target_train)

    return model


def train(input_csv, model_output):
    """
    Train a RandomForest model on the stars dataset from file.

    Parameters:
    - input_csv: Path to the preprocessed stars CSV file.
    - model_output: Path to save the trained model.
    """
    data_frame = pd.read_csv(input_csv)
    model = train_model(data_frame)
    joblib.dump(model, model_output)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Train a model on the stars dataset"
    )
    parser.add_argument(
        "input_csv", type=str, help="Path to preprocessed stars CSV file"
    )
    parser.add_argument(
        "model_output", type=str, help="Path to save the trained model"
    )
    args = parser.parse_args()
    train(args.input_csv, args.model_output)
