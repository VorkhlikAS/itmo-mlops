"""
Tests for the stars dataset preprocessing, training, and evaluation.
"""

import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from src.preprocess import preprocess_df
from src.train import train_model
from src.evaluate import evaluate_df


def test_preprocess():
    """
    Test the preprocessing step.
    """
    data = {
        "color": ["Red", "Blue", "Green", "Red", "Green"],
        "temperature": [5000, 6000, 5500, 5800, 5400],
        "size": [1, 2, 1, 3, 2],
        "Star category": [0, 1, 0, 1, 0],
    }
    data_frame = pd.DataFrame(data)
    processed_df = preprocess_df(data_frame)
    assert not processed_df.empty
    assert "color_Red" in processed_df.columns
    assert "temperature" in processed_df.columns
    assert "size" in processed_df.columns
    assert "Star category" in processed_df.columns


def test_train():
    """
    Test the training step.
    """
    data = {
        "color": ["Red", "Blue", "Green", "Red", "Green"],
        "temperature": [5000, 6000, 5500, 5800, 5400],
        "size": [1, 2, 1, 3, 2],
        "Star category": [0, 1, 0, 1, 0],
    }
    data_frame = pd.DataFrame(data)
    processed_df = preprocess_df(data_frame)
    model = train_model(processed_df)
    assert isinstance(model, RandomForestClassifier)


def test_evaluate():
    """
    Test the evaluation step.
    """
    data = {
        "color": ["Red", "Blue", "Green", "Red", "Green"],
        "temperature": [5000, 6000, 5500, 5800, 5400],
        "size": [1, 2, 1, 3, 2],
        "Star category": [0, 1, 0, 1, 0],
    }
    data_frame = pd.DataFrame(data)
    processed_df = preprocess_df(data_frame)
    model = train_model(processed_df)
    accuracy, report = evaluate_df(processed_df, model)
    assert isinstance(accuracy, float)
    assert "precision" in report


if __name__ == "__main__":
    test_preprocess()
    test_train()
    test_evaluate()
