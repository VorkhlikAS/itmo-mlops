import argparse
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, classification_report
import joblib
import mlflow
import click


def setup_mlflow(mlflow_url: str, mlflow_experiment_name: str):
    mlflow.set_tracking_uri(mlflow_url)
    mlflow.set_experiment(mlflow_experiment_name)
    mlflow.autolog()


@click.command()
@click.option("--data_frame", required=True, help="Input file path", type=click.Path())
@click.option("--mlflow_url", "-ml", required=True, help="MLflow tracking URI", type=str)
@click.option("--experiment_name", "-en", required=True, help="MLflow experiment name", type=str)
@click.option("--run_name", "-rn", required=True, help="MLflow run name", type=str)
def train_model(data_frame, mlflow_url: str, experiment_name: str, run_name: str):
    """
    Train a RandomForest model on the stars dataset in-memory.

    Parameters:
    - data_frame: Input pandas DataFrame.

    Returns:
    - Trained RandomForest model.
    """
    df = pd.read_csv(data_frame)
    
    features = df.drop(columns=["Star category"])
    target = df["Star category"]

    setup_mlflow(mlflow_url, experiment_name)
    with mlflow.start_run(run_name=run_name):
        (
        features_train,
        features_test,
        target_train,
        target_test,
        ) = train_test_split(features, target, test_size=0.2, random_state=42)
        model = RandomForestClassifier(n_estimators=100, random_state=42)
        model.fit(features_train, target_train)
        
        target_pred = model.predict(features_test)
        
        accuracy = accuracy_score(target_test, target_pred)
        report = classification_report(target_test, target_pred, output_dict=True)
        f1_score = report["weighted avg"]["f1-score"]
        precision = report["weighted avg"]["precision"]
        recall = report["weighted avg"]["recall"]
        mlflow.log_metric("accuracy", accuracy)
        mlflow.log_metric("f1", f1_score)
        mlflow.log_metric("recall", recall)
        mlflow.log_metric("precision", precision)
        
        
if __name__ == "__main__":
    train_model()
