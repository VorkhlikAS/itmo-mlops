"""
Evaluate the trained model on the stars dataset.
"""

import argparse
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, classification_report
import joblib


def evaluate_df(data_frame, model):
    """
    Evaluate the trained model on the stars dataset in-memory.

    Parameters:
    - data_frame: Input pandas DataFrame.
    - model: Trained RandomForest model.

    Returns:
    - Accuracy and classification report as a string.
    """
    features = data_frame.drop(columns=["Star category"])
    target = data_frame["Star category"]

    _, features_test, _, target_test = train_test_split(
        features, target, test_size=0.2, random_state=42
    )
    target_pred = model.predict(features_test)
    accuracy = accuracy_score(target_test, target_pred)
    report = classification_report(target_test, target_pred)

    return accuracy, report


def evaluate(input_csv, model_input, output_report):
    """
    Evaluate the trained model on the stars dataset from file.

    Parameters:
    - input_csv: Path to the preprocessed stars CSV file.
    - model_input: Path to the trained model.
    - output_report: Path to save the evaluation report.
    """
    data_frame = pd.read_csv(input_csv)
    model = joblib.load(model_input)
    accuracy, report = evaluate_df(data_frame, model)

    with open(output_report, "w", encoding="utf-8") as report_file:
        report_file.write(f"Accuracy: {accuracy}\n")
        report_file.write(report)

    print(f"Accuracy: {accuracy}")
    print(report)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Evaluate the trained model on the stars dataset"
    )
    parser.add_argument(
        "input_csv", type=str, help="Path to preprocessed stars CSV file"
    )
    parser.add_argument(
        "model_input", type=str, help="Path to the trained model"
    )
    parser.add_argument(
        "output_report", type=str, help="Path to save the evaluation report"
    )
    args = parser.parse_args()
    evaluate(args.input_csv, args.model_input, args.output_report)
